let
  pkgs = import <nixpkgs> {};

  bender = with pkgs.python3Packages; buildPythonPackage rec {
    name = "bender-${version}";
    version = "0.0.3";
    src = pkgs.fetchurl {
   
      url = "https://pypi.python.org/packages/packages/a9/31/9f064b8ecf0da883c2736e96150766c7c1032f095479a7c45f004533a40f/bender-0.0.3.tar.gz";
      md5 = "6218952e92ebffcbc9b5341b70e9155f";
    };
    propagatedBuildInputs = with pkgs;
    [ python3Packages.requests2
      python3Packages.redis
      python3Packages.future ];
    doCheck = false;
  };

  raven = with pkgs.python3Packages; buildPythonPackage rec {
    name = "raven-${version}";
    version = "5.12.0";
    src = pkgs.fetchurl {
      url = "https://pypi.python.org/packages/source/r/raven/raven-5.12.0.tar.gz";
      md5 = "af9d327e5fb16579a6479c58a97e9d15";
    };
    propagatedBuildInputs = with pkgs;
     [ python3 ];
    doCheck = false;
  };

  drae = with pkgs.python3Packages; buildPythonPackage rec {
    name = "drae-${version}";
    version = "0.2.0";
    src = pkgs.fetchurl {
      url = "https://pypi.python.org/packages/21/02/4288e3cc4e93de83864aa15b15f28c0e95593013cea9c114eeb31bfa00bd/drae-0.2.0.tar.gz";
      md5 = "a024fb7c40f865b312aba73b611875fc";
    };
    propagatedBuildInputs = with pkgs;
     [ python3
       python3Packages.lxml
       python3Packages.beautifulsoup4
       python3Packages.requests2
       python3Packages.future ];
    doCheck = false;
  };
in
{ stdenv ? pkgs.stdenv }:

pkgs.python3Packages.buildPythonPackage {
  name = "def_bot";
  version = "0.1.6";
  src = if pkgs.lib.inNixShell then null else ./.;
  propagatedBuildInputs = with pkgs;
   [ python3
     python3Packages.blinker
     python3Packages.flask
     python3Packages.gunicorn
     python3Packages.click
     raven
     drae
     bender ];
}
